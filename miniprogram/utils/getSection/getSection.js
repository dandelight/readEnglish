// async function getSection (col, sectionStr) {
//   // let that = this;
//   const db = wx.cloud.database();
//   await db.collection(col).where({
//     section: sectionStr
//   }).get({}).then((res) => {
//     console.log(res.data[0]);
//      // Section Object
//   });
// }

function getSection(collection, sectionTitle) {
  return new Promise((resolve, reject) => {
    const db = wx.cloud.database();
    db.collection(collection).where({
      section: sectionTitle
    }).get({}).then((res) => {
      // console.log(res.data[0])
      resolve(res.data[0])
    })
  })
}

module.exports = {
  getSection
}