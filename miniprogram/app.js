//app.js
App({
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: 'library-dandelight',
        traceUser: true,
      })
    }
    this.globalData = {
      // 有必要保留这个字段吗?
      // books: []
    }

    // let that = this;
    /*--------------------------------------------------+
     | 这个that很重要, 指向onLaunch函数, 
     | 如果在__collection__.get里调用this, 
     | 指向的将是__collection__
     +--------------------------------------------------*/
     
     
    // const db = wx.cloud.database()
    // const bookList = db.collection('bookList')
    // bookList.get({}).then((res)=>{
    //   console.log(res);
    //   that.globalData.books = res.data;
    // })s
  }
})


// console.log(App);
// console.log(Page);
// console.log(Component);