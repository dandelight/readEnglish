const db = wx.cloud.database();
const bookList = db.collection('bookList');
const globalData = getApp().globalData;

Page({
  onShareAppMessage() {
    return {
      title: '',
      path: ''
    }
  },
  onShareTimeline() {
    return {
      title: '读英语背单词',
      path: '/miniprogram/pages/read/index/'
    }
  },
  data: {    
  books: []
    },
  onLoad(options) {
    // 使用Promise进行更新
    const that = this;
    bookList.get({}).then((res)=>{
      console.log(res);
      globalData.books = res.data;
      that.setData({
        books: res.data
      })
    })
  },
})
