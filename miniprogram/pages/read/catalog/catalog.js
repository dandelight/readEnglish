// miniprogram/pages/catalog/catalog.js
const globalData = getApp().globalData

Page({
  data: {
    bookName: '',
    toc: [],
    col: ''
  },
  onLoad: function(options) {
    console.log(options)
    this.setData({
      bookName: options.bookName,
      col: options.col
    })
    wx.setNavigationBarTitle({
      title: options.bookName
    })		
    // const books = gd.books;
    // const bookName = this.data.bookName
    const toc = globalData.books.find(el => el.title === this.data.bookName).toc
    this.setData({
      toc
    })
  },
  onShow: function() {
    console.log(`In toc.js, there are ${globalData.books.length} books`);
  },
  onShareAppMessage: function() {

  }
})