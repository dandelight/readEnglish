const globalData = getApp().globalData;
const {getSection} = require("../../../utils/getSection/getSection")

Page({
  data: {
    content:''
  },
  onLoad: function (options) {
    // console.log(globalData)
    // this.setData ({
    //   content: globalData.passage
    // })
    const {col, section} = options;
    console.log(col, section)
    getSection(col, section).then((res) => {
      console.log("This is some Information.")
      console.log(res)
      this.setData({
        content: res.content
      })
    })
  },

  // onReady: function () {},
  // onShow: function () {},
  // onHide: function () {},
  // onUnload: function () {},
  // onPullDownRefresh: function () {},
  // onReachBottom: function () {},
  // onShareAppMessage: function () {}
})