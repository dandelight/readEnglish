> "为什么要做这个小程序?"
> "因为想做."

## 设计理念

本程序采用了与同类阅读软件不同的途径: 其他软件玩命地想把你"拉下水", 转移你的注意力,
而本程序致力于减少阅读过程中的干扰, 做到纯净阅读.

另一个启发来自"草莓工作法"(因奇妙的版权问题不提原来的名字了), 每次阅读时间设计为15
分钟, 15分钟后休息, 保障了学习过程的专注, 免受其它社交软件的干扰(distract), 也
保证了用眼健康.

## 软件开发

由于本人学艺未精, 展现在您面前的界面是如此简朴, 不过也歪打正着地减少了页面上的干扰.

逻辑上使用`JavaScript`语言, 采用`Promise`语法执行多种加载任务, 不仅实现了在不同网
络环境下都在加载完成时即展现的效果, 同时使代码简洁明畅, 行云流水.

采用了一定的模块化开发技术, 实现了代码的简明与高效复用.

后端采用小程序云开发技术, 降低了维护成本, 同时通过本地缓存减少了每次加载的不必要的
流量消耗.

`node_modules`依赖包采用`yarn`取代`npm`进行包管理.

## 版权相关

本程序所有书籍均为公版图书, 图书资源来自古腾堡计划(The Guttenburg Project),
在此表示感谢.

前端部分代码来自[`Color UI小程序组件库`](https://github.com/weilanwl/ColorUI),
作者[文晓港](https://www.weilanwl.com/).

以下为之前版本的`README`.

同时值得一提的有:

1. 之前的`.git`目录不要了, 从头再来

2. 将缩进从<kbd>Tab</kbd>全部调节为两个<kbd>space</kbd>.

------

# 总说明

改行了!做好事不留名!

Copyleft 

本程序样式库参考了[ColorUI](https://github.com/weilanwl/ColorUI)以及微信官方[小程序示例](https://github.com/wechat-miniprogram/miniprogram-demo)
其余部分均由本人独立制作


## 日志

### 2020.7.9
我又回来维护了.
本次目的: 
1. 
2. 关键部分上云.

### 2020.6.10-12
* 连续三天
* 取得阶段性成果
* 可以发布alpha版了

## 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

